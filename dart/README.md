# Dart TDD base code

## Setup

Go through the [setup instructions](https://dart.dev/get-dart) for Dart to install the necessary
dependencies.

## How do I launch the tests ?

Execute the tests with:
```
$ pub run test
```

## Where do I write my code ?

Write your code in the folder `lib` or in the folder `test` if you want to write a test.
