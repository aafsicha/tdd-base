import 'package:dart/dojo.dart';
import 'package:test/test.dart';

void main() {
  test("should display 'Hello world !'", () {
    expect("Hello world !", Dojo().hello());
  });
}
