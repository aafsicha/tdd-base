# Typescript TDD base code

Uses mocha with [chai](https://www.chaijs.com/api/bdd/) framework to describe tests.  
Install them first with `npm install`.

Write your code in a new `.ts` file in the `src/` folder, add tests in `test/main.spec.ts`.

Run tests with `npm run test` (or for less verbose output, `npm run test:min`).  
To watch for file changes automatically, run `npm run test:watch` (or for less verbose output, `npm run test:min:watch`).

Note: Typescript is configured in _strict_ mode in `tsconfig.json`.
