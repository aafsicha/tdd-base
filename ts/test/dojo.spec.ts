import { expect } from "chai";
import "mocha";

// Example test. Adapt and complete
// Uses mocha with chai framework:
// https://www.chaijs.com/api/bdd/
describe("Hello world", () => {
    it("says hello", () => {
        const res = "Hello world";

        expect(res.toLowerCase()).to.include("hello");
    });
});
