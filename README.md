# TDD base code

Use code in this repository to setup a quick Test-Driven Development (TDD) environment in various programming languages.

Contributions are welcome: feel free to improve existing code, or to add support for new languages!
