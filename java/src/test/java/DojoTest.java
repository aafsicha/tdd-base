import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DojoTest {

    @Test
    public void shouldBeEqualToHelloWorld() {
        assertEquals("Hello world !", new Dojo().hello());
    }
}