# Java TDD base code

## Setup

Go through the [setup instructions](https://exercism.io/tracks/java/installation) for Java to install the necessary
dependencies.

## How do I launch the tests ?

Execute the tests with:
```
$ ./gradlew test
```
Use `gradlew.bat` if you're on Windows.

## Where do I write my code ?

Write your code in `src/main/java` or in `src/test/java` if you want to write a test.

## Dependencies

The project is using [JUnit 4](https://junit.org/junit4/).