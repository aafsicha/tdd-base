extern crate rspec;
use rspec::{describe, run};

use dojo;

#[test]
pub fn suite() {
    // Example test. Adapt and complete
    // Uses rspec framework:
    // https://github.com/rust-rspec/rspec
    run(&describe("Hello world", "Hello world!", |ctx| {
        ctx.it("says hello", |s| {
            assert!(s.to_lowercase().contains("hello"))
        })
    }));
}
