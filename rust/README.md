# Rust TDD base code

Uses [rspec](https://github.com/rust-rspec/rspec) framework to describe tests.

Write your code in `src/lib.rs` file, add tests in `tests/dojo.rs`.

Run tests with `cargo test`.
